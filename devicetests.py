"""
Tests that can be run against a device or port on a device to provide
additional information.
"""

import requests
import sys

sys.dont_write_bytecode = True

import kanban

class DeviceTest:
    """
    Extend this class to perform a test on a discovered device.
    """
    def __init__(self, hostname, card, labels):
        self.__hostname: str = hostname
        self.__card: kanban.Card = card
        self.__labels: dict[str, kanban.Label] = labels
    
    @property
    def hostname(self) -> str:
        """IP address or hostname of host."""
        return self.__hostname
    
    @property
    def card(self) -> kanban.Card:
        """
        Reference to the Card object associated with the host. Use 
        self.card.add_label(label) to add a label.
        """
        return self.__card
    
    @property
    def labels(self) -> dict[str, kanban.Label]:
        """
        Dictionary mapping strings to Label objects associated with the current Board.
        Example: self.labels["Web"] is a Label object with "Web" as the display name.
        """
        return self.__labels
    
    def run(self):
        """
        Runs the port test. Do not override this function.
        """
        return self.test()
    
    def test(self):
        """
        Your test code will go here.
        """
        raise NotImplementedError("Please override DeviceTest.test() before calling run().")


class PortTest:
    """
    Extend this class to perform a test on a port. This class will be called once
    per found port.
    
    Try to make sure the code here takes less than 3 seconds to run. Assuming 75 
    ports are found and each scan takes 3 seconds, the scan will take over 
    4 additional minutes to run.
    """
    def __init__(self, hostname, port, card, labels):
        self.__hostname: str = hostname
        self.__port: int = port
        self.__card: kanban.Card = card
        self.__labels: dict[str, kanban.Label] = labels
    
    @property
    def hostname(self) -> str:
        """IP address or hostname of host."""
        return self.__hostname
    
    @property
    def port(self) -> str:
        """The port that will be tested."""
        return self.__port
    
    @property
    def card(self) -> kanban.Card:
        """
        Reference to the Card object associated with the host. Use 
        self.card.add_label(label) to add a label.
        """
        return self.__card
    
    @property
    def labels(self) -> dict[str, kanban.Label]:
        """
        Dictionary mapping strings to Label objects associated with the current Board.
        Example: self.labels["Web"] is a Label object with "Web" as the display name.
        """
        return self.__labels
    
    def run(self):
        """
        Runs the port test. Do not override this function.
        """
        return self.test()
    
    def test(self):
        """
        Your test code will go here.
        """
        raise NotImplementedError("Please override PortTest.test() before calling run().")



##### Tests that will be run once per device #####




##### Test that will be run once per port #####

class HTTPServerTest(PortTest):
    """
    Checks whether there is a Web server listening on a port.
    """
    def test(self):
        # Try to connect to the port, with 2.5-second timeout to see if there is an HTTP(s) listener.
        http_connection_successful = True
        try:
            requests.get("http://" + self.hostname + ":" + str(self.port) + "/", timeout=2.5)
        except (requests.exceptions.ConnectionError, requests.exceptions.SSLError, requests.exceptions.ReadTimeout):
            try:
                requests.get("https://" + self.hostname + ":" + str(self.port) + "/", verify=False, timeout=2.5)
            except (requests.exceptions.ConnectionError, requests.exceptions.SSLError, requests.exceptions.ReadTimeout):
                http_connection_successful = False
        if http_connection_successful:
            self.card.add_label(self.labels["Web"])