"""
Script to upload Nmap scans to Nextcloud, written by Yuliang Huang and UFSIT.

Currently supports scanning one host only.

### Prerequisites
 - Install the requests module with `pip install requests`
 - Make sure nmap is accessible from $PATH

### Notes
 - This program should not be used on untrusted filesystems, it is susceptible to fatal race conditions.
 - This is a VERY noisy program and should only be used if stealth is optional.

### Copying

Copyright (c) 2023 Yuliang Huang and contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

"""

import collections
import json
import os
import pathlib
import random
import requests
import subprocess
import sys
import typing
import xml.etree.ElementTree as XmlET

sys.dont_write_bytecode = True

import devicetests
import kanban

from nextcloudparams import SUBDIRECTORY_NAME

class Port:
    """
    Represents an port on a host
    """
    def __init__(self, number: int, protocol: str):
        if not isinstance(number, int):
            raise TypeError("Parameter \"number\" must be an integer")
        if not isinstance(protocol, str):
            raise TypeError("Parameter \"protocol\" must be a string")
        self.__number = number
        self.__protocol = protocol
    
    @property
    def number(self) -> int:
        """Number of this port"""
        return self.__number
    @property
    def protocol(self) -> str:
        """Protocol of this port"""
        return self.__protocol
    
    def __eq__(self, other):
        return (self.__number, self.__protocol) == (other.__number, other.__protocol)
    
    def __hash__(self):
        return hash(self.__str__())
    
    def __repr__(self):
        return "Port (" + str(self.number) + ", " + self.protocol + ")"
    
    def __str__(self):
        return str(self.number) + '/' + self.protocol


class NmapService:
    """
    Represents a service discovered by Nmap
    """
    def __init__(self, name: str, description: str, confidence: int):
        self.name = name.strip()
        self.description = description.strip()
        self.confidence = confidence
    
    def auto_string(self) -> str:
        """Automatically select the most suitable string to represent this NmapService."""
        return_string = ""
        if self.description:
            return_string = self.description
        elif self.name:
            return_string = self.name
        if return_string:
            # Confidence modifiers
            if self.confidence < 3:
                return_string += " (??)"
            elif self.confidence < 8:
                return_string += " (?)"
        return return_string
    
    def __repr__(self):
        return "NmapService (" + repr(self.name) + ", " + repr(self.description) + ", " + repr(self.confidence) + ")"
    


class Host:
    """
    Represents a host on a network
    """
    def __init__(self, ip: str, hostname: str = None):
        self.ip = ip
        self.hostname = hostname
        self.open_ports = []
    
    def __repr__(self):
        return "Host (" + self.ip + ", " + repr(self.hostname) + ", " + repr(self.open_ports) + ")"


def main(argv: list[str]) -> int:
    
    if len(argv) < 2:
        print("Missing required argument <TARGET>")
        print("Usage example: " + argv[0] + " 127.0.0.1")
        # print("Note that <TARGET> can be a CIDR subnet, like 192.0.2.0/24")
        return 1
    
    scan_target = argv[1]
    """The target to scan"""
    
    # Create a working subdirectory if it doesn't exist
    pathlib.Path(SUBDIRECTORY_NAME).mkdir(parents=True, exist_ok=True)
    
    print("Scanning for live hosts on " + scan_target + "...")
    
    host_discovery_xml_file = SUBDIRECTORY_NAME + os.sep + scan_target.replace('/', "-") + ".xml"
    subprocess.run(["nmap", "-sn", "-oX", host_discovery_xml_file, scan_target])
    
    # Append all found hosts to list
    hosts_to_scan: list[str] = []
    host_discovery_xml = XmlET.parse(host_discovery_xml_file).getroot()
    for host in host_discovery_xml.findall("host"):
        ip_address_xml = host.find("address")
        if host.find("status").get("state").lower() == "up" and ip_address_xml is not None:
            # Host is up
            hosts_to_scan.append(ip_address_xml.get("addr"))
    
    # Create and share a Nextcloud board
    devices_board = kanban.Board(name="Target network " + scan_target)
    """Nextcloud Kanban board to store information on all the live hosts on the network"""
    devices_board.add_share(1, "cptcfinals2024", True, True, True)
    
    # Create labels (tags) to automatically tag cards with.
    device_port_labels: dict[str, kanban.Label] = {}
    """Labels that can be applied to a device or port for extra information."""
    device_port_labels["Linux"] = devices_board.add_label("Linux")
    device_port_labels["Unix"] = devices_board.add_label("Unix")
    device_port_labels["Web"] = devices_board.add_label("Web")
    device_port_labels["Windows"] = devices_board.add_label("Windows")
    
    # TODO Add cards for no shell, user shell, root shell
    # TODO Remove default tags
    
    loop_count = 0  # Number of times we've been through the for loop
    
    # Now scan each host, adding to Nextcloud as we go.
    for host_to_scan in hosts_to_scan:
        print("Now performing TCP scan of the top 100 ports on " + host_to_scan + "...")
        
        nmap_output_filename = SUBDIRECTORY_NAME + os.sep + host_to_scan + ".nmap"
        nmap_xml_filename = SUBDIRECTORY_NAME + os.sep + host_to_scan + ".xml"
        subprocess.run(["nmap", "-sV", "-oX", nmap_xml_filename, "-o", nmap_output_filename, host_to_scan])
        
        xml_root = XmlET.parse(nmap_xml_filename).getroot()
        
        # First get start and end times of the scan
        start_time: str = xml_root.get("startstr")
        end_time: str = "Unknown"
        try:
            end_time = xml_root.find("runstats").find("finished").get("timestr")
        except AttributeError:
            print("WARNING: No end time found for current run!")
        
        ip_address = ""
        hostname = host_to_scan  # Default hostname to IP address
        open_ports: collections.OrderedDict = collections.OrderedDict()
        """Dict mapping each port to a service. Maps a Port to an NmapService."""
        os_type_votes: dict[str, int] = {}
        """Number of "votes" for each OS type"""
        for host in xml_root.findall("host"):
            ip_address = host.find("address").get("addr")
            if host.find("hostnames").find("hostname") is not None:
                hostname = host.find("hostnames").find("hostname").get("name")
            ports_xml = host.find("ports")
            for port_xml in ports_xml.findall("port"):
                detected_service = NmapService("", "", -1)
                if port_xml.find("service") is not None:
                    service_xml = port_xml.find("service")
                    detected_service = NmapService(service_xml.get("name", ""), service_xml.get("product", ""), 
                            int(service_xml.get("conf", "0")))
                    
                    # Cast a vote for the OS type
                    if service_xml.get("ostype"):
                        port_os_type = service_xml.get("ostype")  # OS type for this port
                        if port_os_type in os_type_votes:
                            os_type_votes[port_os_type] += 1
                        else:
                            os_type_votes[port_os_type] = 1
                if port_xml.find("state").get("state").lower() == "open":
                    open_ports[Port(int(port_xml.get("portid")), port_xml.get("protocol"))] = detected_service
        
        os_type = "Unknown"
        """The type of OS most likely running on the target, if known."""
        most_voted_os_types: list = []
        current_highest_votes = 0
        for os_type_candidate in os_type_votes:
            if os_type_votes[os_type_candidate] > current_highest_votes:
                most_voted_os_types.clear()
            if os_type_votes[os_type_candidate] >= current_highest_votes:
                most_voted_os_types.append(os_type_candidate)
                current_highest_votes = os_type_votes[os_type_candidate]
        if len(most_voted_os_types) == 1:
            # Decide on an OS type if there is no tie and at least one OS was identified.
            os_type = most_voted_os_types[0]
        
        print("Scan complete, now uploading to Nextcloud...")

        current_stack = devices_board.add_stack(host_to_scan + " (" + hostname + ")", loop_count * 2)
        
        card_number = 1
        scan_info_card = current_stack.add_card("Scan Information", card_number, 
                "Start time: " + start_time + "\n\nEnd time: " + end_time)
        if os_type in device_port_labels:
            scan_info_card.add_label(device_port_labels[os_type])
        else:
            scan_info_card.description += "\n\nOS type: " + os_type
        
        # Tests that will be applied once per device for extra information.
        
        
        card_number += 1
        nmap_raw_scan = "```\n"
        with open(nmap_output_filename, 'r') as nmap_raw_output_file:
            nmap_raw_scan += nmap_raw_output_file.read()
        nmap_raw_scan += "\n```"
        raw_scan_card = current_stack.add_card("Raw scan", card_number, nmap_raw_scan)
        card_number += 1
        for port in open_ports:
            current_card = current_stack.add_card(str(port) + " - " + open_ports[port].auto_string(), card_number)
            
            # Tests that will be run against each port for extra information.
            devicetests.HTTPServerTest(host_to_scan, port, current_card, device_port_labels).run()
            
            card_number += 1
        
    print("Success.")
    
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
