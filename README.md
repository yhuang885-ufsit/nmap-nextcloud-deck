# Nmap Nextcloud Deck
Automatically uploads port scan data to Nextcloud Decks.

## Prerequisites
 - Python 3.8+ installed
 - Install the requests module with `pip install requests`
 
## Installation
1. Clone this repository with `git clone <REPOSITORY>`
2. Create a file named `nextcloudauth.py` with the following content, replacing the domain and sample credentials with your information:
```
"""
Nextcloud authentication credentials.
"""

NEXTCLOUD_URL = "https://nextcloud.domain.example/"
"""Base URL for Nextcloud. Include the trailing slash at the end."""

NEXTCLOUD_USERPASS = ("your_username", "your_password")
"""Tuple of (username, password) for authentication with Nextcloud"""
```
3. Run `python3 nmapautomator.py`

## Other notes
For security, do **not** publish the `nextcloudauth.py` file on GitHub, GitLab, or anywhere else on the Internet. The best practice is to keep the entry present in the `.gitignore` file to prevent accidental committing of the file.
