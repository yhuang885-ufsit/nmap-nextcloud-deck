"""
Abstractions for the Nextcloud Deck Kanban board. Use these
classes to interact with Nextcloud Deck.
"""

import functools
import random
import sys
import time

sys.dont_write_bytecode = True

from nextcloudauth import NEXTCLOUD_USERPASS

# Keeping this for now but eventually will use environment variables
from nextcloudauth import MYSQL_HOST
from nextcloudauth import MYSQL_USER
from nextcloudauth import MYSQL_PASS
from nextcloudauth import MYSQL_DB
import mysql.connector

# mysql connector, define it once for now

mydb = mysql.connector.connect(
    host=MYSQL_HOST,
    user=MYSQL_USER,
    password=MYSQL_PASS,
    database=MYSQL_DB
)

class Board:
    """
    Represents a Kanban board.
    """
    
    def __init__(self, name: str = None, board_id: int = None):
        """
        :param board_id: The ID of the board on the Nextcloud server. If None, will create a new board.
        :raises FileNotFoundError: If the board with the specified ID does not exist.
        """
        if board_id is None:
            # Create a board with a random color and share it to everyone.
            # id, name, owner, color, archived, deleted_at, last_modified
            
            mycursor = mydb.cursor()
            sql = "INSERT INTO oc_deck_boards VALUES (NULL, %s, %s, %s, %s, %s, %s)"
            val = (name, NEXTCLOUD_USERPASS[0], hex(random.randint(0x0, 0xffffff))[2:], 0, 0, int(time.time()))
            mycursor.execute(sql, val)

            mydb.commit()

            self.board_id: int = mycursor.lastrowid
            self.stacks: list[Stack] = []
            self.labels: list[Label] = []
        else:
            self.board_id: int = board_id
            
            # Get the board.
            mycursor = mydb.cursor()
            sql = "SELECT * FROM oc_deck_boards WHERE id = %s"
            val = (board_id,)
            mycursor.execute(sql, val)

            boardResult = mycursor.fetchall()
            
            # Error-checking
            if len(boardResult) == 0:
                raise FileNotFoundError("Kanban board with ID " + str(board_id) + 
                        " not found on server, or the current user has insufficient permissions.")

            # Get and update the labels
            self.labels: list[Label] = []

            mycursor = mydb.cursor()
            sql = "SELECT * FROM oc_deck_labels WHERE board_id = %s"
            val = (board_id,)
            mycursor.execute(sql, val)

            labelResult = mycursor.fetchall()

            for label in labelResult:
                self.labels.append(Label(self.board_id, label["id"], label["title"]))
            
            # Get and update the stacks
            self.stacks: list[Stack] = []

            mycursor = mydb.cursor()
            sql = "SELECT * FROM oc_deck_stacks WHERE board_id = %s"
            val = (board_id)
            mycursor.execute(sql, val)

            stackResult = mycursor.fetchall()

            for stack in stackResult:
                correct_index = len(self.stacks)
                for i in reversed(range(len(self.stacks))):
                    if stack["order"] < existing_stack.stack_order:
                        # We're at the correct location to insert
                        correct_index = i
                        break
                self.stacks.insert(correct_index, stack(self.board_id, stack["id"]))
    
    def add_label(self, name: str):
        """
        Adds a label to the board.
        :param name: Name of the label to add
        :return: The newly added label, as a Label object
        """

        # id, title, color, board_id, last_modified
        mysqlcursor = mydb.cursor()
        sql = "INSERT INTO oc_deck_labels VALUES (NULL, %s, %s, %s, %s)"
        val = (name, hex(random.randint(0x0, 0xffffff))[2:], self.board_id, int(time.time()))
        mysqlcursor.execute(sql, val)
        mydb.commit()

        newly_created_label = Label(self.board_id, mysqlcursor.lastrowid, name)
        self.labels.append(newly_created_label)
        return newly_created_label
    
    # TODO Add functionality to delete labels, and delete the default labels in the Deck.
    
    def add_stack(self, name: str, order: int = None):
        """
        Add a stack to this board.
        :param name: Name (title) of the stack.
        :param order: The order that the stack will be arranged in. If None, will use a random 
        integer (and is liable to collisions if more than 500 thousand stacks are desired). Ignored if
        a value is given for stack_id.
        :return: The newly created stack of cards, as a Stack object
        """
        newly_created_stack = Stack(self.board_id, name=name, order=order)
        self.stacks.append(newly_created_stack)
        return newly_created_stack
    
    def add_share(self, share_type: int, participant: str, edit: bool, 
            share: bool = False, manage: bool = False):
        """
        Add sharing permissions.
        :param share_type: Type of collaborator: 0 = user, 1 = group, 7 = circle
        :param participant: UID of the participant
        :param edit: Whether to give edit permissions
        :param share: Whether to give share permissions. Optional, defaults to False.
        :param manage: Whether to give manage permissions. Optional, defaults to False.
        :return ID of the newly added sharing rule (ACL rule)
        """

        # id, board_id, type, participant, permission_edit, permission_share, permission_manage
        mysqlcursor = mydb.cursor()
        sql = "INSERT INTO oc_deck_board_acl VALUES (NULL, %s, %s, %s, %s, %s, %s)"
        val = (self.board_id, share_type, participant, edit, share, manage)
        mysqlcursor.execute(sql, val)
        mydb.commit()

        return mysqlcursor.lastrowid 


class Label:
    """
    Represents a label for a board.
    """
    def __init__(self, board_id: int, label_id: int, name: str):
        """
        :param board_id: The ID of the board on the Nextcloud server that this label is associated with.
        :param name: Name (title) of the label. Required if label_id is None.
        :param label_id: The ID of the label on the Nextcloud server. If None, will create a new stack.
        """
        if name is None:
            raise ValueError("Parameter \"name\" is required and cannot be None.")
        self.board_id = board_id
        self.label_id = label_id
        self.name = name
    

@functools.total_ordering
class Stack:
    """
    Represents a stack in a Kanban Board. The comparison operators are overloaded to be the stack order.
    This means that two equivalent stacks may have different content.
    """
    def __init__(self, board_id: int, name: str = None, order: int = None, stack_id: int = None):
        """
        :param board_id: The ID of the board on the Nextcloud server that this stack is associated with.
        :param name: Name (title) of the stack. Required if stack_id is None.
        :param order: The order that the stack will be arranged in. If None, will use a random 
        integer (and is liable to collisions if more than 500 thousand stacks are desired). Ignored if
        a value is given for stack_id.
        :param stack_id: The ID of the stack on the Nextcloud server. If None, will create a new stack.
        
        :raises ValueError: If both name and stack_id are None
        """
        self.board_id = board_id
        if stack_id is None:
            # Create a new stack
            if name is None:
                raise ValueError("Either the parameter \"name\" or the parameter \"stack_id\" must be present")
            if order is None:
                order = random.randint(1, 2147483647)

            # id, title, board_id, order, deleted_at, last_modified
            mycursor = mydb.cursor()
            sql = "INSERT INTO oc_deck_stacks VALUES (NULL, %s, %s, %s, %s, %s)"
            val = (name, self.board_id, order, 0, int(time.time()))
            mycursor.execute(sql, val)
            mydb.commit()

            self.name: str = name
            """Name (title) of the stack"""
            self.stack_id = mycursor.lastrowid
            self.order = order
            self.cards: list[Card] = []
            """List of cards in the current stack"""
        else:
            # Get the stack
            mycursor = mydb.cursor()
            sql = "SELECT * FROM oc_deck_stacks WHERE id = %s"
            val = (stack_id,)
            mycursor.execute(sql, val)

            stackResult = mycursor.fetchall()

            # Error-checking
            if len(stackResult) == 0:
                raise FileNotFoundError("Kanban stack with ID " + str(stack_id) + 
                        " not found on server, or the current user has insufficient permissions.")

            # TODO Check for unexpected status code, like error 500 or 504.
            
            self.stack_id = stack_id
            self.name = stackResult[0]["title"] 
            self.cards: list[Card] = []

            """List of cards in the current stack"""
    
            mycursor = mydb.cursor()
            sql = "SELECT * FROM oc_deck_cards WHERE stack_id = %s"
            val = (stack_id,)
            mycursor.execute(sql, val)

            cardResult = mycursor.fetchall()

            for card in cardResult:
                self.cards.append(Card(self.board_id, self.stack_id, card["id"], card["title"], card["order"], card["description"]))
    
    @property
    def title(self) -> str:
        return self.name
    @title.setter
    def title(self, value: str):
        self.name = value
    
    def add_card(self, name: str, order: int = None, description: str = None):
        """
        Add a card to the current stack.
        :param name: Name (title) of the card to be added.
        :param order: The order that the card will be arranged in. If None, will use a random 
        integer (and is liable to collisions if more than 500 thousand stacks are desired).
        :param description: Description of the card. If None, the description will be left blank.
        :raises TypeError: If parameter "name" is not a string
        """
        if not isinstance(name, str):
            raise TypeError("Parameter \"name\" must be of type string, not \"" + str(type(name)) + "\"")
        if order is None:
            order = random.randint(1, 2147483647)
        if description is None:
            description = ""

        # id, title, description, description_prev, stack_id, type, last_modified, last_editor, created_at, owner, order, archived, duedate, notified, deleted_at
        mycursor = mydb.cursor()
        sql = "INSERT INTO oc_deck_cards VALUES (NULL, %s, %s, NULL, %s, 'plain', %s, NULL, %s, %s, %s, 0, NULL, 0, 0)"
        val = (name, description, self.stack_id, int(time.time()), int(time.time()), NEXTCLOUD_USERPASS[0], order)
        mycursor.execute(sql, val)
        mydb.commit()

        newly_created_card = Card(self.board_id, self.stack_id, 
                mycursor.lastrowid, name, order, description)
        self.cards.append(newly_created_card)
        return newly_created_card
    
    
    # Comparison operators
    def __lt__(self, other):
        return self.order < other.order
    def __eq__(self, other):
        return self.order == other.order
        
    def __repr__(self):
        return "KanbanStack(" + str(self.board_id) + ", " + str(self.stack_id) + ", " + str(self.order) + ")"


@functools.total_ordering
class Card:
    """
    Represents a card in a Kanban Board. The comparison operators are overloaded to be the cards order.
    This means that two equivalent cards may have different content.
    
    Additional attributes of the cards are lazily loaded in as needed
    """
    def __init__(self, board_id: int, stack_id: int, card_id: int, name: str, order: int, description: str = None):
        """
        :param board_id: The ID of the board on the Nextcloud server that this card is associated with.
        :param stack_id: The ID of the stack on the Nextcloud server that this card is associated with.
        :param card_id: The ID of the card on the Nextcloud server. If None, will create a new card.
        :param name: Name (title) of this card.
        :param order: The order that the card is be arranged in.
        :param labels: List of labels of the card. Defaults to empty list
        :param description: A description for this card. If None, the description will be left blank.
        """
        if description is None:
            description = ""
        self.board_id = board_id
        self.stack_id = stack_id
        self.card_id = card_id
        self.name = name
        self.order = order
        self.__labels: list[Label] = []
        self.description = description
    
    def add_label(self, label: Label):
        """
        Add a label to the card (requires a Label object). Has no effect if label is already on the card.
        :param label_id: Label to add to the card
        """
        self.labels  # Update the internal labels variable.
        # First check to see if the card is already labeled.
        for label in self.__labels:
            if label.label_id == label_id:
                # Label is already present!
                return
        
        # id, label_id, card_id
        mysqlcursor = mydb.cursor()
        sql = "INSERT INTO oc_deck_assigned_labels VALUES (NULL, %s, %s)"
        val = (label.label_id, self.card_id)
        mysqlcursor.execute(sql, val)
        mydb.commit()
        
        self.__labels.append(label)
    
    # TODO Allow the description to be edited (and synced to Nextcloud)
    
    @property
    def labels(self):
        """
        Get the labels for this card. Also updates the private __labels variable.
        """

        mycursor = mydb.cursor()
        sql = "SELECT * FROM oc_deck_assigned_labels WHERE card_id = %s"
        val = (self.card_id,)
        mycursor.execute(sql, val)

        labelResult = mycursor.fetchall()

        # Clear the list and update with newest label values.
        self.__labels.clear()
        for labels in labelResult:
            # Get the label
            mycursor = mydb.cursor()
            sql = "SELECT * FROM oc_deck_labels WHERE id = %s"
            val = (labels["label_id"],)
            mycursor.execute(sql, val)

            label_json = mycursor.fetchall()[0]

            self.__labels.append(Label(label_json["id"], label_json["title"]))
        
        return self.__labels
    
    @property
    def title(self) -> str:
        return self.name
    @title.setter
    def title(self, value: str):
        self.name = value
    
    # Comparison operators
    def __lt__(self, other):
        return self.order < other.order
    def __eq__(self, other):
        return self.order == other.order
    
    def __repr__(self):
        return "KanbanCard(" + repr(self.board_id) + ", " + repr(self.stack_id) + ", " + repr(self.card_id) + \
                ", " + repr(self.name) + ", " + repr(self.order) + ", " + repr(self.description) + ")"
